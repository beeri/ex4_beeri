#pragma once

#include "OutStream.h"

class OutStreamEncrypted : public OutStream
{
private:
	int _shift;
public:
	OutStreamEncrypted(const int shift);
	~OutStreamEncrypted();

	OutStream& operator<<(char *str);
	OutStream& operator<<(int num);
};

