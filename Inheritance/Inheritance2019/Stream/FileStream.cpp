#include "FileStream.h"



FileStream::FileStream(const char* path)
{
	this->file = fopen(path, "w");
}


FileStream::~FileStream()
{
	fclose(this->file);
}

void FileStream::endline()
{
	fprintf(this->file, "\n");
}