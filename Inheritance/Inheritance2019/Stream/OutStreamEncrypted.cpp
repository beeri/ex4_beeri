#include "OutStreamEncrypted.h"
#include <string.h>
 

OutStreamEncrypted::OutStreamEncrypted(const int shift)
{
	this->_shift = shift;
}


OutStreamEncrypted::~OutStreamEncrypted()
{

}

OutStream& OutStreamEncrypted::operator<<(char *str)
{
	for (int i = 0; i < strlen(str); i++)
	{
		if (str[i] <= 126 && str[i] >= 32)
		{
			if (str[i] + this->_shift > 126)
			{
				str[i] += this->_shift;
				str[i] = str[i] - 94;
			}
			str[i] += this->_shift;
		}
	}

	OutStream::operator<<(str);

	return *this;
}

OutStream& OutStreamEncrypted::operator<<(int num)
{
	
	num += this->_shift;

	OutStream::operator<<(num);
	return *this;
}
