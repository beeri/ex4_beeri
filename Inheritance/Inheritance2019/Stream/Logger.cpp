#include "Logger.h"

unsigned int Logger::_counter = 0;

Logger::~Logger()
{

}

void Logger::print(const char* msg)
{
	if (this->_logToScreen)
	{
		_counter++;
		this->_fileStream << _counter;
		this->_fileStream << " - ";
		this->_fileStream << msg;
		endline();

		this->_outStream << _counter;
		this->_outStream << " - ";
		this->_outStream << msg;
		endline(); 
	}
	else
	{
		_counter++;
		this->_fileStream << _counter;
		this->_fileStream << " - ";
		this->_fileStream << msg;
		endline();
	}
}