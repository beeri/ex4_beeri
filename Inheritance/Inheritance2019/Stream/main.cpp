#include "OutStream.h"
#include "FileStream.h"
#include "OutStreamEncrypted.h"
#include "Logger.h"
#define _CRT_SECURE_NO_WARNINGS


int main(int argc, char **argv)
{
	/*   PART A
	FileStream a("C:\\Users\\user\\Downloads\\txtFile.txt");
	
	a << "�I am the Doctor and I�m ";
	a << 1500 << " years old�";
	endline();
	a << &endline;
	*/
	
	/* PART B
	char str[] = { "bbb" };
	OutStreamEncrypted a(4);
	a << str;
	a << 111;
	endline();
	*/

	/*PART C
	Logger log("C:\\Users\\user\\Downloads\\txtFile.txt", true);
	Logger log2("C:\\Users\\user\\Downloads\\txtFile2.txt", true);
	log.print("i love a");
	log2.print("i love b");
	log.print("i love c");
	log2.print("i love d");
	log.print("i love e");
	*/


	return 0;
}
