#pragma once
#include "FileStream.h"

class Logger
{
private:
	OutStream _outStream;
	FileStream _fileStream;
	bool _logToScreen;
	static unsigned int _counter;

public:
	Logger::Logger(const char *filename, bool logToScreen) :
		_fileStream(filename)
	{
		this->_logToScreen = logToScreen;
		
	}

	~Logger();

	void print(const char *msg);
};
