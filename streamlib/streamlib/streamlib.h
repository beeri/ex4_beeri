#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
namespace mls
{
	class OutStream
	{
	protected:


	public:
		OutStream();
		~OutStream();

		OutStream& operator<<(const char* str);
		OutStream& operator<<(int num);
		OutStream& operator<<(void(*pf)());
	};
}
void endline();